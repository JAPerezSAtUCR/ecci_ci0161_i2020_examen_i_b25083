package cql.ecci.ucr.ac.examen_I_b25083;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cql.ecci.ucr.ac.examen_I_b25083.R;


public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> itemName;
    private final ArrayList<Integer> imgId;
    private final ArrayList<String> itemDescription;
    public CustomListAdapter(Activity context, ArrayList<String> itemName, ArrayList<Integer> imgId, ArrayList<String>
            itemDescription) {
        super(context, R.layout.lista_personalizada, itemName);
        this.context = context;
        this.itemName = itemName;
        this.imgId = imgId;
        this.itemDescription = itemDescription;
    }
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.lista_personalizada, null, true);
        TextView name = (TextView) rowView.findViewById(R.id.name);
        ImageView image = (ImageView) rowView.findViewById(R.id.icon);
        TextView description = (TextView) rowView.findViewById(R.id.description);
        name.setText(itemName.get(position));
        image.setImageResource(imgId.get(position));
        description.setText(itemDescription.get(position));
        return rowView;
    }
}