package cql.ecci.ucr.ac.examen_I_b25083;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import cql.ecci.ucr.ac.examen_I_b25083.R;

public class MainActivity extends AppCompatActivity {

    //definimos lista de juegos
    private ListView listView;

    private ArrayList<String> tableTopName;
    private ArrayList<String> tableTopDescription;
    private ArrayList<Integer> tableTopIcon;
    private ArrayList<String> tableTopFullInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        insertGame1();
        insertGame2();
        insertGame3();
        insertGame4();
        insertGame5();

        // Obtenemos la lista
        listView = (ListView) findViewById(R.id.list);


        // Crear una instancia de cada uno de los ArrayList
        tableTopName = new ArrayList<>();
        tableTopIcon = new ArrayList<>();
        tableTopDescription = new ArrayList<>();
        tableTopFullInfo = new ArrayList<>();

        // Leo cada uno de los juegos para agregarlos a la lista
        readTableTop("TT001");
        readTableTop("TT002");
        readTableTop("TT003");
        readTableTop("TT004");
        readTableTop("TT005");

        // definimos el adaptador para la lista
        CustomListAdapter adapter = new CustomListAdapter(this, tableTopName, tableTopIcon, tableTopDescription);
        // asignamos el adaptador al ListView
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, tableTopFullInfo.get(position), Toast.LENGTH_SHORT).show();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                goToMap(tableTopName.get(position));
                return true;
            }
        });
    }

    // Se insertan los datos del juego 1
    private void insertGame1() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT001",
                "Catan",
                "1995",
                "Kosmos",
                "Germany",
                48.774538,
                9.188467,
                "Picture yourself in the era of discoveries:after a long voyage of great deprivation,your ships have finally reached the coast ofan uncharted island. Its name shall be Catan!But you are not the only discoverer. Otherfearless seafarers have also landed on theshores of Catan: the race to settle theisland has begun!",
                "3-4",
                "10+",
                "1-2 hours"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Se insertan los datos del juego 2
    private void insertGame2() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT002",
                "Monopoly",
                "1935",
                "Hasbro",
                "United States",
                41.883736,
                -71.352259,
                "The thrill of bankrupting an opponent, but it pays to play nice, because fortunes could change with the roll of the dice. Experience the ups and downs by collecting property colors sets to build houses, and maybe even upgrading to a hotel!",
                "2-8",
                "8+",
                "20-180 minutes"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Se insertan los datos del juego 3
    private void insertGame3() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT003",
                "Eldritch Horror",
                "2013",
                "Fantasy Flight Games",
                "United States",
                45.015417,
                -93.183995,
                "An ancient evil is stirring. You are part ofa team of unlikely heroes engaged in an international struggle to stop the gathering darkness. To do so, you’ll have to defeat foul monsters, travel to Other Worlds, andsolve obscure mysteries surrounding thisunspeakable horror.",
                "1-8",
                "14+",
                "2-4 hours"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Se insertan los datos del juego 4
    private void insertGame4() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT004",
                "Magic: the Gathering",
                "1993",
                "Hasbro",
                "United States",
                41.883736,
                -71.352259,
                "Magic: The Gathering is a collectible and digital collectible card game created byRichard Garfield. Each game of Magic represents a battle between wizards known as planeswalkers who cast spells, use artifacts,and summon creatures.",
                "2+",
                "13+",
                "Varies"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Se insertan los datos del juego 5
    private void insertGame5() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT005",
                "Hanabi",
                "2010",
                "Asmodee",
                "France",
                48.761629,
                2.065296,
                "Hanabi—named for the Japanese word for\"fireworks\"—is a cooperative game in whichplayers try to create the perfect fireworks show by placing the cards on the table in theright order.",
                "2-5",
                "8+",
                "25 minutes"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }


    private void readTableTop(String id) {
        // Instancio el objeto
        TableTop tableTop = new TableTop();

        // Leo la fila
        tableTop.read(getApplicationContext(), id);


        //Crear un string con toda la informacion correspondiente al juego
        String fullInfo = "id: " + tableTop.getId() + " \n name: " + tableTop.getName() + "\n year: " + tableTop.getYear()
                + "\n publisher: " + tableTop.getPublisher() + "\n country: " + tableTop.getCountry() + "\n latitude: " + tableTop.getLatitude()
                + "\n longitude: " + tableTop.getLongitude() + "\n description " + tableTop.getDescription() + "\n numPlayers: " + tableTop.getNumPlayers()
                + "\n ages: " + tableTop.getAges() + "\n playingTime: " + tableTop.getPlayingTime();

        // LLeno la lista de juegos
        tableTopDescription.add(tableTop.getDescription());
        tableTopName.add(tableTop.getName());
        tableTopFullInfo.add(fullInfo);

        // Dependiendo del ID del juego le asigna la imagen correspondiente
        switch (id) {
            case "TT001":
                tableTopIcon.add(R.drawable.catan);
                break;
            case "TT002":
                tableTopIcon.add(R.drawable.monopoly);
                break;
            case "TT003":
                tableTopIcon.add(R.drawable.eldritch);
                break;
            case "TT004":
                tableTopIcon.add(R.drawable.mtg);
                break;
            case "TT005":
                tableTopIcon.add(R.drawable.hanabi);
                break;
        }
    }

    // Intent para ver la localización en el mapa
    private void goToMap(String id){
        TableTop tableTop = new TableTop();

        tableTop.read(getApplicationContext(), id);

        String url = "geo:" + tableTop.getLatitude() + "," +
                tableTop.getLongitude();
        String q = "?q="+ tableTop.getLatitude() + "," +
                tableTop.getLongitude() + "(" + tableTop.getName() + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url + q));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }
}
